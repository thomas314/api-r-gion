$.getJSON("https://geo.api.gouv.fr/regions", function(data) {
  // console.log(data[0]);
  for (var i = 0; i < data.length; i++) {
    var regions = data[i].nom;
    var coderegion = data[i].code;
    var donnees = { index: regions };
    var template = "<option>{{index}}</option>";
    var output = Mustache.render(template, donnees);
    $("#selection").append(output);
  }
});
